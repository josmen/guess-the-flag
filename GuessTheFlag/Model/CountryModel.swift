import Foundation

struct CountryModel: Decodable {
    let name: String
    let code: String
    
    var flag: String {
        let base: UInt32 = 127397
        var s = ""
        for v in code.unicodeScalars {
            s.unicodeScalars.append(UnicodeScalar(base + v.value)!)
        }
        return String(s)
    }
    
    static func loadJson() -> [CountryModel]? {
        guard let url = Bundle.main.url(forResource: "Countries",
                                        withExtension: "json") else {
            return nil
        }
        
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let countries = try decoder.decode([CountryModel].self, from: data)
            return countries
        } catch {
            return nil
        }
    }
}
