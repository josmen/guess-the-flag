//
//  GuessTheFlagApp.swift
//  GuessTheFlag
//
//  Created by Jose Antonio on 15/8/22.
//

import SwiftUI

@main
struct GuessTheFlagApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
