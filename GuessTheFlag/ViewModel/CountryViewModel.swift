import Foundation

class CountryViewModel: ObservableObject {
    @Published var countries = [CountryModel]()
    
    init() {
        countries = CountryModel.loadJson() ?? []
    }
}
